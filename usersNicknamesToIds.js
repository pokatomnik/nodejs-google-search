'use strict';

var async = require('async');
var request = require('request');
var headers = require('./headers');

const baseUrl = 'http://api.vkontakte.ru/method/users.get?'+
	'fields=uid,first_name,last_name,nickname,screen_name,sex,bdate,city'+
	',country,timezone,photo,photo_medium,photo_big,has_mobile,rate,contacts,'+
	'education,online,counters&uids=';

var usersNicknamesToIds = function (nicknames) {
	var result = [];
	return new Promise(function (resolve, reject) {
		var tasks = [];
		for (let i=0; i<nicknames.length; i++) {
			tasks.push(function (callback) {
				request(
					{
						url : `${baseUrl}${nicknames[i].slice(1)}`,
						headers : headers
					},
					function done(error, reponse, body) {
						console.log(body);
						var resultJSON = JSON.parse(body).response[0];
						//console.log(`${baseUrl}${nicknames[i].slice(1)}`);
						callback(null, resultJSON);
					}
				);
			});
		}
		async.series(tasks, function doneNicknamesToIdsTranslation(err, result) {
			if (err) {
				console.log(err);
			} else {
				resolve(result);
			}
		});
	});
}

module.exports = usersNicknamesToIds;