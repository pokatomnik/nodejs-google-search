/**
 * Created by pokat_000 on 05.04.2016.
 */

function exactlyVideo (href, badWords) {
    var result = true;
    for (var index=0; index<badWords.length; index++) {
        var badWord = badWords[index];
        if (href.indexOf(badWord) !== -1) {
            result = false;
        }
    }

    return result;
}

module.exports = exactlyVideo;