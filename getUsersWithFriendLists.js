'use strict';

var request = require('request');
var async = require('async');
var settings = require('./settings');
var headers = require('./headers');

const apiUrl = 'https://api.vk.com/method/friends.get?fields=nickname,domain,'+
	'sex,bdate,city,country,timezone,photo_50,photo_100,photo_200_orig,has_mobile,'+
	'contacts,education,online,relation,last_seen,status,can_write_private_message,'+
	'can_see_all_posts,can_post,universities&user_id=';

var getUsersWithFriendLists = function (usersArray) {
	return new Promise(function (resolve, reject) {
		var tasks = [];
		for (let i=0; i<usersArray.length; i++) {
			tasks.push(function (callback) {
				request(
					{
						url : `${apiUrl}${usersArray[i].uid}`,
						headers : headers
					}, 
					function done (error, response, body) {
						var userWithFriendList = usersArray[i];
						userWithFriendList.friendList = JSON.parse(body).response;
						// console.log(userWithFriendList);
						setTimeout(
							callback.bind(this, null, userWithFriendList),
							settings.timeToWaitBeforeNextQuery
						);
					}
				);
			});
		}

		async.series(tasks, function (error, result) {
			resolve(result);
		});
	});
}

module.exports = getUsersWithFriendLists;