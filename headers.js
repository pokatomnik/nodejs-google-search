'use strict';

// browser's headers''
var headers = {
    //"Connection" : "keep-alive",
    "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Upgrade-insecure-requests" : "1",
    // Must be mobile useragent (no script page)
    "User-agent" : "Mozilla/5.0 (Android; Mobile; rv:13.0) Gecko/13.0 Firefox/13.0",
    "Dnt" : "1",
    "Referer" : "https://www.google.ru/",
    "Accept-language" : "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2"
};

module.exports = headers;