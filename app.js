#!/usr/bin/env node
/**
 * Created by pokatomnik on 04.04.2016.
 */
'use strict';

var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var jsonFile = require('jsonfile');
var google = require('./google');
var exactlyVideo = require('./exactlyVideo');
var headers = require('./headers');
var usersNicknamesToIds = require('./usersNicknamesToIds');
var settings = require('./settings');
var getUsersWithFriendLists = require('./getUsersWithFriendLists');


// сам запрос в гугл, параметризировать, #ToDo!
const mySearchQuery = '"вечный жид"';

// минус слова, которые есть в url, но по некоторым причинам не подходящие
// Первый означает плейлист, второй - редирект. Ни то ни другое не нужно
const badWords = ['?list', 'away', 'club'];

// selector for user's href quering
const selector = '.si_row a';

// Сколько результатов ищем
google.resultsPerPage = 25;

// Язык всегда по умолчанию русский
google.lang = 'ru';

// Текст ссылки на следующую страницу, необходимо параметризировать, #ToDO!
google.nextText = 'Следующая';


/**
 * Гуглим запрос, огарничиваем поиск по сайту vk.com
 */
// setTimeout(function () {
//     var videos = [ 
//         'https://m.vk.com/video214339916_165709929',
//         'http://m.vk.com/video68373390_140447618',
//         'https://m.vk.com/video144272759_166021110',
//         'http://m.vk.com/video3766358_164256110',
//         'http://m.vk.com/video177875851_164590978',
//         'http://m.vk.com/video99768825_165087824',
//         'http://m.vk.com/video20417776_77817212',
//         'http://m.vk.com/video71799581_162318811',
//         'https://m.vk.com/video175038258_166089699',
//         'http://m.vk.com/video68373390_140447613',
//         'http://m.vk.com/video65533303_160851699',
//         'http://m.vk.com/video52785203_164335612',
//         'https://m.vk.com/video2417865_168590148',
//         'http://m.vk.com/video43384919_138308819',
//         'http://m.vk.com/video80594885_162318457',
//         'http://m.vk.com/video43384919_138308823',
//         'http://m.vk.com/video68096107_165558521',
//         'http://m.vk.com/video-29438844_160798260',
//         'http://m.vk.com/video43384919_138308825',
//         'http://m.vk.com/video68373390_139631341' ];
google(`site:https://vk.com ${mySearchQuery}`, function (err, res) {
    if (err) {
        console.log(err);
    } else {
        var videos = [];
        for (var i=0; i<res.links.length; i++) {
            var href = res.links[i].href.replace('vk.com', 'm.vk.com');
            if (exactlyVideo(href, badWords)) {
                videos.push(href);
            }
        }
//         console.log(videos);
        var tasksIndex = [];
        for (let i=0; i<videos.length; i++) {
            tasksIndex.push(function (callback) {
                console.log(`Trying to request video with id : ${i}; video\'s href: ${videos[i]}...`);
                request(
                    {
                        url: videos[i].replace('https://', 'http://'),
                        pool : {
                            maxSockets : Infinity
                        },
                        timeout : 60000,
                        followRedirect : true,
                        followAllRedirects : true,
                        headers : headers
                    }, 
                    function (error, response, body) {
                        console.log(`Got response of video with id: ${i}, video\'s href: ${videos[i]}`);
                        try {
                            var $ = cheerio.load(body);
                            var userHref = $(selector).attr('href');
                            console.log(userHref);
                            var result = {
                                error : error,
                                response : response,
                                body : body,
                                videoHref : videos[i],
                                userHref : userHref,
                                number : i
                            };
                            console.log('---------------------');
                            setTimeout(callback.bind(this, null, result), settings.timeToWaitBeforeNextQuery);
                        } catch (e) {
                            console.log(error);
                            console.warn('SOMETHING GOES WRONG WITH NETWORK, PLEASE RESTART APP.JS');
                            var result = {
                                exception : e,
                                error : error || {},
                                response : response || {},
                                body : body || {},
                                videoHref : videos[i],
                                userHref : '',
                                number : i
                            };
                            console.log('---------------------');
                            setTimeout(callback.bind(this, null, result), settings.timeToWaitBeforeNextQuery);
                        }

                    }
                );
            });
        }

        async.series(tasksIndex, (err, result) => {
            const outPath = './out.json';
            var purePersons = [];
            for (var personFromDirtyQueryIndex=0; personFromDirtyQueryIndex<result.length; personFromDirtyQueryIndex++) {
                var personFromDirtyQuery = result[personFromDirtyQueryIndex];
                if ((typeof personFromDirtyQuery.userHref === 'string') &&
                    (personFromDirtyQuery.userHref.length > 1)) {
                    purePersons.push(personFromDirtyQuery.userHref);
                }
            }
            var purePersonsWithoutClub = [];
            for (var i=0; i<purePersons.length; i++) {
                if (purePersons[i].indexOf('club') === -1) {
                    purePersonsWithoutClub.push(purePersons[i]);
                }
            }
            usersNicknamesToIds(purePersonsWithoutClub).then(function (result) {
                console.log(result);
                getUsersWithFriendLists(result).then(function (usersWithFriendLists) {
                    jsonFile.writeFile(outPath, usersWithFriendLists, {spaces : 4}, console.log.bind(console, 'all done'));
                });
            });
        });
    }
});
// }, 100);
